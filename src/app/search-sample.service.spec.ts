import { TestBed, inject } from '@angular/core/testing';

import { SearchSampleService } from './search-sample.service';

describe('SearchSampleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchSampleService]
    });
  });

  it('should be created', inject([SearchSampleService], (service: SearchSampleService) => {
    expect(service).toBeTruthy();
  }));
});
