import { any } from 'codelyzer/util/function';
import { List } from 'immutable';

import {Serialize, SerializeProperty, Serializable} from 'ts-serializer';


@Serialize({})
//If we want to use a different name for the dto to be serialized against altogether we can use below
// @Serialize({
//   root: 'coconfig'
// })
export class Config extends Serializable {
  @SerializeProperty({
    //root: 'CustUrl'
  })
  customersUrl: String;

  @SerializeProperty()
  filesUrl: List<any>;

}
