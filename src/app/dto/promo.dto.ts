import { Serialize, Serializable, SerializeProperty } from 'ts-serializer';
@Serialize({})
export class Promotion extends Serializable {
  @SerializeProperty()
  id: number;

  @SerializeProperty()
  name: string;

  @SerializeProperty()
  description: string;


  constructor(id, name, description) {
    super();
    this.id = id;
    this.name = name;
    this.description = description;
  }


}
