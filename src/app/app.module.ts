
import { Config } from './dto/config.dto';
import { SearchService } from './search.service';

import { RouterModule, Routes, Router } from '@angular/router';
import { FormAppComponent } from './form-app/form-app.component';
import { ConfigService } from './config.service';
import { TokenServiceService } from './token-service.service';

import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { Component, NgModule } from '@angular/core';
import { Form, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ConfigServiceComponent } from './config-service/config-service.component';
import { SearchPromoComponent } from './search-promo/search-promo.component';
import { APP_BASE_HREF } from '@angular/common';
import { HeaderComponent } from './header/header.component';

export const routes: Routes = [

  {path : '' , redirectTo: 'form', pathMatch: 'full'},
  {path : '', component : FormAppComponent},
  {path : 'search', component: SearchPromoComponent},
  {path : 'form', component: FormAppComponent},
  //adding a parameterized link param
  {path : 'search/:term', component: SearchPromoComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ConfigServiceComponent,
    FormAppComponent,
    SearchPromoComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes, {useHash:true})
  ],
  providers: [ConfigService, TokenServiceService, SearchService, Config],
  bootstrap: [AppComponent]
})
export class AppModule { }
