
import { Response } from '@angular/http';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {Promotion} from './dto/promo.dto';
import { List } from 'immutable';
import {map, filter, reduce, switchMap, tap} from 'rxjs/operators';

/*
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/pipe';

these are dot operator imports which gets patched to Observable.prototype
say when a operator is not used but import statement remains, then the operators patched to Observable.pattern
remains (removing unused is called DOM tree shaking)

To overcome this from Rxjs 5.5 pipepable operators are introduced
. they can be imported as above
they are pure functions and hence Observable.prototype will not be patched
 * npm install immutable to install immutable moduel
 * if no ts file found , then rename the .js to ts and then import it as above
 */

@Injectable()
export class SearchService {
  getUrl: String = './../assets/promotionList.json';
  subject: BehaviorSubject<Promotion[]> = new BehaviorSubject([]); // initialize with an empty response[]
  subjectAsObservable;
   someResult;
   promoList:Promotion[];
  constructor(private http: HttpClient) {
    this.getPromoList();
    console.log("after first");
    this.getPromoValues();
    console.log("after second call");
  }
  results1;
    getPromoList(){
    // by default it emits a response of Json and hence Observabpe<PromotionList>
    // throws an error
    this.someResult = this.http.get(`${this.getUrl}`);
    console.log("before map"+<Observable<Promotion[]>> this.someResult);
    return (<Observable<Promotion[]>> this.someResult);
    //console.log("first subsriber"+JSON.stringify (this.someResult);

  }

  getPromoValues(){

    // now http.get returns an observable
    // so use of Observable.from(this.http.get(SOME_ULR)) will give another observable
    // basically an observable<observable<Promotion[]>>>
    // so just using as it is
     this.getPromoList()
      .pipe(tap(data => console.log("inside tap"+ data)))
      .pipe(map( data => {
        console.log("SDSD:", data);
        return data;

      }))
      .subscribe(
        resp => {
          console.log("Inside resp"+JSON.stringify(resp));
          this.promoList = <Promotion[]>resp;
          console.log(this.promoList);
          return this.promoList;
        },
        err => {
          console.log(err);
          return err;
        },
        () => console.log('Completed')
      );


  }
  // getPromoListValues() {
  //   this.getPromoList().
  //   map( (data:Response) => {
  //     console.log(<List<Promotion>> data.json());
  //     this.promoList = new PromotionList();
  //     console.log("isnide maod"+JSON.stringify(data));
  //     this.promoList.deserialize(data);
  //     console.log("another call"+this.promoList);
  //     return this.promoList;
  //   }).
  //   subscribe(
  //     data => {
  //     return data;
  //     },
  //     err => console.log('Error thrown by observable'+err)
  //   );
  // }

  private handleError(err: Response){
    return Observable.throw(err.statusText);

  }
}
