import { ActivatedRoute } from '@angular/router';
import { setTimeout } from 'timers';


import { Response } from '@angular/http';
import { _switch } from 'rxjs/operator/switch';
import { any } from 'codelyzer/util/function';
import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import {map, flatMap, filter, switchMap, tap} from 'rxjs/operators';

import { Promotion } from './../dto/promo.dto';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SearchService } from './../search.service';
import { AfterViewChecked, AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';


@Component({
  selector: 'app-search-promo',
  templateUrl: './search-promo.component.html',
  styleUrls: ['./search-promo.component.css']
})
export class SearchPromoComponent implements  AfterViewInit {
  searchGroup: FormGroup;
  stringify  =  require('json-stringify-safe');
  someResult: any;
  promoList: Promotion[];
  resp: Response;
  localInput = new FormControl('', Validators.required);
  consumedPromoList: Observable<Promotion[]>;
  loading: Boolean = false;

  // Use activated route to get the data from
  constructor(private searchService: SearchService, fb: FormBuilder, private paramConfig: ActivatedRoute) {
    this.searchGroup = fb.group({
      'localInput': this.localInput
    });
     this.stringify = require('json-stringify-safe');

    this.paramConfig.params.subscribe(params => console.log("link param:", params));

     this.searchService.getPromoList().subscribe(
       resp => {
         this.someResult = <Promotion[]>resp;
         console.log("Inside sub in comp"+this.stringify(resp));
         console.log("before calling the methid");
         this.callDto(<Promotion[]>resp);
       }
     );
     console.log('inside const()' + this.stringify(this.someResult));
   }

callDto(data){
    console.log("caling"+data);
    this.someResult = <Promotion[]>data;
    console.log("Now priting:"+this.someResult);
    this.anotherMethod();
  }

  anotherMethod(){
    this.promoList = <Promotion[]> this.someResult;
    console.log("Inside another method"+this.stringify(this.someResult));
    console.log("also printing promoList:"+this.promoList);
  }

    ngAfterViewInit(){
      setTimeout( function(){
        console.log('inside setTimeout()' + this.someResult );
      }, 5000);
    }


    ngAfterViewContentChecked() {
    console.log('inside AfterViewContent()' + this.stringify(this.someResult));
  }


}
