
import {ReactiveFormsModule, FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import { selector } from 'rxjs/operator/publish';
import { Component } from '@angular/core';
import 'rxjs/Rx';
@Component({
  selector: 'app-form',
  templateUrl: './form-app.component.html',
//   template: `<form #userlogin = "ngForm" (ngSubmit) = "onClickSubmit(userlogin.value) [ngFormOptions]="{updateOn: 'blur'}"" >
//    <input type = "text" name = "emailid" placeholder = "emailid" ngModel>
//    <br/>
//    <input type = "password" name = "passwd" placeholder = "passwd" ngModel>
//    <br/>
//    <input type = "submit" value = "submit">
// </form>`,
  styleUrls: ['./form-app.component.css']
})

export class FormAppComponent {

  // IN template

  // We can also provide the ngFormOptions at the form level or at every HTML group level.


  // There are two types of forms 1. Template driven and other Module(Reactive) driven
  // Template driven most of the work is done in the templates
  // Below is the module driven form and hence work is done in the component
  // if template driven we add ngModel to the HTML tag attribute and nothing is added in component
  // we also add "ngform" and describe the data that is passed as userlogin
  // Above template defines a template driven form


  // ONE IMPORTANT CHANGE
  // App.moduole.ts should contain ReactiveFormsModule added to its list of modules


  // and to gain control over each tag, we use formcontrol
  // the property name in the component is mapped to HTML using formcontrolName in HTML and just FormControl in component
  // we can define validators if required

  //data binded in html with formControlName


// ALSO AN IMPORTANT POINT TO NOTE: A HTML element attribute is different from a DOM property
// Angular updates the DOM properties not HTML attributes
// For example, when the browser renders <input type="text" value="Bob">, it creates a corresponding DOM node with a value property initialized to "Bob".

// When the user enters "Sally" into the input box, the DOM element value property becomes "Sally". But the HTML value attribute remains unchanged as you discover if you ask the input element about that attribute: input.getAttribute('value') returns "Bob".

// The HTML attribute value specifies the initial value; the DOM value property is the current value.

// The disabled attribute is another peculiar example. A button's disabled property is false by default so the button is enabled. When you add the disabled attribute, its presence alone initializes the button's disabled property to true so the button is disabled.

// Adding and removing the disabled attribute disables and enables the button. The value of the attribute is irrelevant, which is why you cannot enable a button by writing <button disabled="false">Still Disabled</button>.

// Setting the button's disabled property (say, with an Angular binding) disables or enables the button. The value of the property matters.

// The HTML attribute and the DOM property are not the same thing, even when they have the same name.


  formData: FormGroup;
  localComment = new FormControl('', Validators.required);
  localName = new FormControl('', Validators.required);
  localEmail = new FormControl('',[ Validators.required, Validators.pattern('[^ @]*@[^ @]*')]);
  lastUpdTs:Date;
  name:string;
  constructor(private formB: FormBuilder){

    this.formData = formB.group({
       'localComment': this.localComment,
       'localEmail': this.localEmail,
       'localName' : this.localName
    });
    //his.localEmail.value.disable();
    this.formData.valueChanges.filter(data => this.formData.valid).
  map(data => {
    this.lastUpdTs = new Date();
    return data;
  }).
    subscribe(data => console.log("Inside subscirbe"+JSON.stringify(data)));
  }
  // formData: FormGroup;
  // comment = new FormControl('', Validators.required);
  // name = new FormControl('', Validators.required);
  // //email = new FormControl('text@text.com', Validators.required);


  // /* Observable Solution */
  // constructor(fb: FormBuilder) {
  //   this.formData = fb.group({
  //     'comment': this.comment,
  //     'name': this.name
  //     //'email': this.email
  //   });
  //   this.formData.valueChanges.subscribe(
  //     data => console.log(JSON.stringify(data))
  //   );
  // }

  onSubmit() {
    console.log("onsubmit clicked"+this.formData);
  }

  OnKeyBoardEvent(event){
    //console.log("On keyup " +event.target.value);
    //this.emailDisabled = false;
    this.localEmail = event.target.value;
    console.log("onkeyboard event"+ this.localEmail);
  }


  OnMouseClick(data: any){
    console.log("onclick data"+data);
    //this.localEmail.enable();

  }
}
