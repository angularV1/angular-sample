
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private routeConfig: Router) {

   }

   /**
    * this is a link param array ,
     We have to give the URL link path configured in the routes in app module
    *
    */
   homeFunc(event: any) {
     console.log("Event in home"+event.target);
     this.routeConfig.navigate(['form']);
   }

   searchFunc(event: any) {
     console.log("Event in search"+event.target);
    this.routeConfig.navigate([ 'search']);
   }

  ngOnInit() {
  }

}
